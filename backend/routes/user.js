const router = require('express').Router();
const UserController = require('../controllers/user');
const auth = require('../controllers/auth');

router.post('/login', (req, res) => {
	const password = req.body.password;
	const email = req.body.email;
	UserController.login({ email, password }).then(result => res.send(result));
})

router.get('/verify-token', auth.verify, (req, res) => {
	res.send({ success: 'valid-token'})
})

router.post('/google-login', (req, res) => {
	UserController.googleLogin(req.body.googleToken)
		.then(result => res.send(result));
})

router.post('/register', (req, res) => {
	const userObject = req.body;
	UserController.register(userObject).then(result => res.send(result));
})

router.get('/', auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	UserController.getUser(userId).then(result => res.send(result));
})

router.put('/', auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	const userObj = req.body;
	UserController.updateUser({ userId, userObj }).then(result => res.send(result));
})

module.exports = router;