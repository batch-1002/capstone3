const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema({
	budgetType: {
		type: String, // expense or income
		required: [true, 'Budget type is required']
	},
	name: {
		type: String,
		required: [true, 'Name is required'],
		unqiue: true
	},
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: [true, 'User is required']
	}
});

module.exports = mongoose.model('Category', categorySchema);