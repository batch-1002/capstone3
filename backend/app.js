const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();

const userRoutes = require('./routes/user');
const categoryRoutes = require('./routes/category');
const recordRoutes = require('./routes/record');

mongoose.connect(process.env.MONGODB_CONNECTION_STRING, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useCreateIndex: true,
	useFindAndModify: false
});

mongoose.connection.once('open', 
	() => console.log('Now connect to MongoDB Atlas.')
);

if (process.env.DEV === 'true') {
	console.log('Dev mode');
	const corsOptions = {
		origin: ['http://localhost:3000'],
		optionsSuccessStatus: 200
	}
	app.use(cors(corsOptions));
}
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/api/users', userRoutes);
app.use('/api/categories', categoryRoutes);
app.use('/api/records', recordRoutes);

app.listen(process.env.PORT || 3001, () => {
	console.log(`API is now online on port ${process.env.PORT || 3001 }`)
});