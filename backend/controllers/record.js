const RecordDataAccess = require('../dataAccess/record');

module.exports.createRecord = async (recordObj) => {
	recordObj.dateCreated = DateControlToEarliestDateObj(recordObj.dateCreated);
	const newRecord = await RecordDataAccess.createRecord(recordObj);
	return { success: 'success'}
}

module.exports.getRecordsByDate = async ({ user, startDateControl, endDateControl }) => {
	const startDate = DateControlToEarliestDateObj(startDateControl);
	const endDate = DateControlToLatestDateObj(endDateControl);
	const query = { user, dateCreated: { $gte: startDate, $lte: endDate } }
	const records = await RecordDataAccess.getRecords(query);
	return { success: records }
}

module.exports.deleteRecordsWithCategoryId = async ({ userId, categoryId }) => {
	const query = { category: categoryId, user: userId }
	await RecordDataAccess.deleteRecords(query);
	return { success: 'success'}
}

module.exports.getRecord = async ({ userId, recordId }) => {
	const query = { user: userId, _id: recordId }
	const record = await RecordDataAccess.getRecord(query);
	return { success: record }
}

module.exports.updateRecord = async ({ userId, recordId, recordObj }) => {
	recordObj.user = userId;
	const query = { user: userId, _id: recordId }
	await RecordDataAccess.updateRecord({ query, recordObj })
	return { success: 'success' }
}

module.exports.deleteRecord = async ({ userId, recordId }) => {
	const query = { user: userId, _id: recordId }
	const res = await RecordDataAccess.deleteRecord(query);
	if (res === true) {
		return { success: 'success' }
	} else if (res === false) {
		return { fail: 'fail' }
	}
}

module.exports.getAllRecords = async (userId) => {
	const query = { user: userId };
	const records = await RecordDataAccess.getRecords(query);
	return { success: records };
}

function DateControlToLatestDateObj(dateControl) {
	let [year, month, date] = dateControl.split('-');
	return new Date(year, parseInt(month) - 1, date, 23, 59);
}

function DateControlToEarliestDateObj(dateControl) {
	let [year, month, date] = dateControl.split('-');
	return new Date(year, parseInt(month) - 1, date, 0, 0);
}