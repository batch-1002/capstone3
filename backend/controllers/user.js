const UserDataAccess = require('../dataAccess/user');
const bcrypt = require('bcryptjs');
const auth = require('./auth');
const { OAuth2Client } = require('google-auth-library');
const clientId = process.env.GOOGLE_CLIENT_ID;

module.exports.login = async ({ email, password }) => {
	const user = await UserDataAccess.getUser({ email });

	if (user === null) {
		return { fail: 'wrong-credentials' }
	}

	if (user.loginType === 'google') {
		return { fail: 'wrong-type' }
	}

	if (!bcrypt.compareSync(password, user.password)) {
		return { fail: 'wrong-credentials' }
	} 

	const token = auth.createAccessToken(user);
	return { success: token }
}

module.exports.register = async (userObj) => {
	const user = await UserDataAccess.getUser({ email: userObj.email });
	if (user === null) {
		userObj.loginType = "email";
		userObj.password = bcrypt.hashSync(userObj.password, 10);
		await UserDataAccess.createUser(userObj);
		return { success: 'success' }
	} else if (user.loginType === 'google') {
		return { fail: 'email-used-for-different-login-type' }
	} else {
		return { fail: 'email-already-registered' }
	}
}

module.exports.getUser = async (userId) => {
	const query = { _id: userId }
	const userDetails = await UserDataAccess.getUser(query);
	return { success: userDetails }
}

module.exports.updateUser = async ({ userId, userObj }) => {
	const query = { _id: userId }
	if (userObj.password === '') {
		delete userObj.password;
	} else {
		userObj.password = bcrypt.hashSync(userObj.password, 10);
	}
	await UserDataAccess.updateUser({query, userObj});
	return { success: 'success' }
}

module.exports.googleLogin = async (googleToken) => {
	const client = new OAuth2Client(clientId);
	const data = await client.verifyIdToken({
		idToken: googleToken,
		audience: clientId
	});
	const googleUser = data.payload;

	// check validity of google account
	if (googleUser.email_verified !== true) return { fail: 'google-auth-error'}

	let dbUser = await UserDataAccess.getUser({ email: googleUser.email });
	
	// create a user with loginType google if none exsists
	if (dbUser === null) {
		const userObj = {
			firstName: googleUser.given_name,
			lastName: googleUser.family_name,
			email: googleUser.email,
			loginType: 'google'
		}
		await UserDataAccess.createUser(userObj);
		dbUser = await UserDataAccess.getUser({ email: googleUser.email });
	} 

	// check that loginType is google
	if (dbUser.loginType !== 'google') {
		return { fail: 'email-used-for-different-login-type' }
	}


	const token = auth.createAccessToken(dbUser);
	return { success: token }
}
