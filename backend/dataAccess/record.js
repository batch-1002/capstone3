const RecordDB = require('../models/Record');

module.exports.createRecord = async (recordObj) => {
	const newRecordObj = new RecordDB(recordObj);
	try {
		await newRecordObj.save();
		return true
	} catch(err) {
		return err
	}
}

module.exports.getRecords = async (query) => {
	return await RecordDB.find(query).populate('category');
}

module.exports.deleteRecords = async (query) => {
	try {
		await RecordDB.deleteMany(query)
		return true;
	} catch(err) {
		return err;
	}
}

module.exports.deleteRecord = async (query) => {
	try {
		await RecordDB.deleteOne(query);
		return true;
	} catch(err) {
		return false;
	}
}

module.exports.getRecord = async (query) => {
	return await RecordDB.findOne(query).populate('category');
}

module.exports.updateRecord = async ({ query, recordObj}) => {
	try {
		await RecordDB.findOneAndUpdate(query, recordObj);
		return true
	} catch(err) {
		return err;
	}
}