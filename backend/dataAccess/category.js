const CategoryDB = require('../models/Category');

module.exports.getCategory = async (query) => {
	return await CategoryDB.findOne(query);
}

module.exports.createCategory = async (categoryObj) => {
	const newCategory = new CategoryDB(categoryObj);
	try {
		await newCategory.save();
		return true;
	} catch(err) {
		return err;
	}
}

module.exports.getCategories = async (query) => {
	return await CategoryDB.find(query);
}

module.exports.updateCategory = async ({ query, categoryObj}) => {
	try {
		await CategoryDB.findOneAndReplace(query, categoryObj);
		return true;
	} catch(err) {
		return err;
	}
}

module.exports.getCategory = async (query) => {
	return await CategoryDB.findOne(query);
}

module.exports.deleteCategory = async (query) => {
	try {
		await CategoryDB.deleteOne(query);
		return true;
	} catch(err) {
		return err;
	}
}