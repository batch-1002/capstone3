import { Form, Row, Col } from 'react-bootstrap';

export default function FromToDateControl({
	startDate, endDate, setStartDate, setEndDate
}) {
	return (
		<Form>
			<Row>
				<Col>
					<Form.Group>
						<Form.Label>From</Form.Label>
						<Form.Control
							type="date"
							value={startDate}
							onChange={(e) => setStartDate(e.target.value)}
						/>
					</Form.Group>
				</Col>
				<Col>
					<Form.Group>
						<Form.Label>To</Form.Label>
						<Form.Control
							type="date"
							value={endDate}
							onChange={(e) => setEndDate(e.target.value)}
						/>
					</Form.Group>
				</Col>
			</Row>
		</Form>
	)
}