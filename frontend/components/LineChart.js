import { Line } from 'react-chartjs-2';

export default function LineChart({ labels, data}) {


	return (
		<Line 
			data={{
					labels,
					datasets: [{
						data,
						fill: true,
      					backgroundColor: 'rgb(255, 99, 132)',
      					borderColor: 'rgba(255, 99, 132, 0.2)',
					}]
			}}
			redraw={false}
			options={{
				plugins: {
					legend: {
						display: false
					}
				}
			}}
		/>
	)
};