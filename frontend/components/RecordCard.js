import { Card, Row, Col, Button } from 'react-bootstrap';
import Link from 'next/link';

export default function RecordCard({ record, deleteRecord }) {
	const budgetColor = record.category.budgetType === 'expense'
		? 'text-danger font-weight-bold'
		: 'text-success font-weight-bold'

	const mathSign = record.category.budgetType === 'expense'
		? '-'
		: '+'

	return (
		<Card className="p-3 my-3">
			<Row>
				<Col>
					<Card.Title>
						{record.description}	
					</Card.Title>	
				</Col>
				<Col className="text-right">
					<span className={budgetColor}>{mathSign + record.amount}</span>
				</Col>
			</Row>
			<Card.Subtitle className="my-1 text-muted">
				<span
					className={budgetColor}
				>
					{
						record.category.budgetType[0].toUpperCase() + 
						record.category.budgetType.slice(1)
					}
				</span>
				{` (${record.category.name})`}
			</Card.Subtitle>
			<Card.Text className="my-2">
				{(new Date(record.dateCreated)).toDateString()}
			</Card.Text>
			<Row>
				<Col>
					<Link href={`/records/edit/${record._id}`}>
						<Button variant="warning">Edit</Button>
					</Link>	
				</Col>
				<Col className="text-right">
					<Button 
						variant="danger"
						onClick={() => deleteRecord(record._id)}
					>
						Delete
					</Button>
				</Col>
			</Row>
		</Card>
	)
}