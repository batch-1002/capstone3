import { Fragment, useContext, useState } from 'react';
import Link from 'next/link';
import { Navbar, Nav } from 'react-bootstrap';
import UserContext from '../contexts/UserContext';

export default function NavBar() {
	const { user } = useContext(UserContext);
	const [expanded, setExpanded] = useState(false);
	return (
		<Navbar expanded={expanded} bg="light" expand="lg">
			<Link href="/">
				<a className="navbar-brand">Budget Tracker</a>
			</Link>
			{ (user.token)
				? <Fragment>
						<Navbar.Toggle aria-controls="basic-navbar-nav" onClick={() => setExpanded(!expanded)} />
						<Navbar.Collapse id="basic-navbar-nav">
							<Nav onClick={() => setExpanded(false)}>
								<Link href="/categories">
									<a role="button" className="nav-link">
										Categories
									</a>
								</Link>

								<Link href="/records">
									<a role="button" className="nav-link">
										Records
									</a>
								</Link>

								<Link href="/charts/balance-trend">
									<a role="button" className="nav-link">
										Balance Trend
									</a>
								</Link>

								<Link href="/charts/breakdown">
									<a role="button" className="nav-link">
										Category Breakdown
									</a>
								</Link>

								<Link href="/profile">
									<a role="button" className="nav-link">
										My Profile
									</a>
								</Link>

								<Link href="/logout">
									<a role="button" className="nav-link">
										Logout
									</a>
								</Link>
							</Nav>
						</Navbar.Collapse>
					</Fragment>
				
				: ''
			}
			
		</Navbar>
	)
}