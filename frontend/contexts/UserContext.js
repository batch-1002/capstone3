import React, { useEffect, useState } from 'react';

const UserContext = React.createContext();

export default UserContext;

export function UserProviderWrapper({ children }) {
	const [user, setUser] = useState({
		token: null
	});

	const backendAPI = process.env.NEXT_PUBLIC_BACKEND_API || '';

	useEffect(() => {
		async function setToken() {
			const token = localStorage.getItem('token');
			if (token === null) return;
			const res = await fetch(`${backendAPI}/api/users/verify-token`, {
				headers: {
					'Authorization': `Bearer ${token}`
				}
			});
			const data = await res.json();

			if (data.success) {
				setUser({ token });
			} else {
				localStorage.removeItem('token');
			}
		}

		setToken();
	}, []);

	useEffect(() => {
		console.log(user.token);
	}, [user])

	const unsetUser = () => {
		localStorage.clear();
		setUser({
			token: null
		});
	}

	return (
		<UserContext.Provider value={{user, setUser, unsetUser}}>
			{ children }
		</UserContext.Provider>
	)
}

