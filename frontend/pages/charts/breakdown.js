import { Fragment, useState, useEffect, useContext } from 'react';
import { Row, Col } from 'react-bootstrap';
import PieChart from '../../components/PieChart';
import FromToDateControl from '../../components/FromToDateControl';
import UserContext from '../../contexts/UserContext';
import dateHelper from '../../helpers/dateHelpers';

export default function breakdown() {
	const { user } = useContext(UserContext);
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');

	const [incomeBreakdown, setIncomeBreakdown] = useState({
		labels: [],
		data: [],
		count: 0
	});
	const [expenseBreakdown, setExpenseBreakdown] = useState({
		labels: [],
		data: [],
		count: 0
	});

	const backendAPI = process.env.NEXT_PUBLIC_BACKEND_API || '';

	useEffect(() => {
		const endDateObj = new Date();
		const endYear = endDateObj.getFullYear();
		const endMonth = parseInt(endDateObj.getMonth());
		const endDay = endDateObj.getDate();

		const startDateObj = new Date(endYear, parseInt(endMonth - 2), endDay);

		setEndDate(dateHelper.dateObjToDateControl(endDateObj));
		setStartDate(dateHelper.dateObjToDateControl(startDateObj));
	}, []);

	useEffect(() => {
		if (startDate !== '', endDate !== '') {
			generateBreakdownData();
		}
	}, [startDate, endDate, user.token])

	async function generateBreakdownData() {
		const token = user.token;
		if (token === null) return;

		const route = `${backendAPI}/api/records/all/${startDate}/${endDate}`;
		const recordsRes = await fetch(route, {
			headers: {
				'Authorization': `Bearer ${token}`
			}
		});
		const recordsData = await recordsRes.json();
		const records = recordsData.success;
		const incomeHash = {};
		const expenseHash = {};

		records.forEach(record => {
			if (record.category.budgetType === 'income') {
				if (incomeHash[record.category.name] === undefined) {
					incomeHash[record.category.name] = 0;
				}
				incomeHash[record.category.name] += record.amount;
			} else if (record.category.budgetType === 'expense') {
				if (expenseHash[record.category.name] === undefined) {
					expenseHash[record.category.name] = 0;
				}
				expenseHash[record.category.name] += record.amount;
			}
		});

		const incomeLabels = [];
		const incomeData = [];
		let incomeCount = 0;
		Object.keys(incomeHash).forEach(key => {
			incomeLabels.push(key);
			incomeData.push(incomeHash[key])
			incomeCount += 1;
		})

		const expenseLabels = [];
		const expenseData = [];
		let expenseCount = 0;
		Object.keys(expenseHash).forEach(key => {
			expenseLabels.push(key);
			expenseData.push(expenseHash[key])
			expenseCount += 1;
		})

		setIncomeBreakdown({
			labels: incomeLabels,
			data: incomeData,
			count: incomeCount
		})

		setExpenseBreakdown({
			labels: expenseLabels,
			data: expenseData,
			count: expenseCount
		})
	};

	return (
		<Fragment>
			<h4>Category Breakdown</h4>
			<FromToDateControl
				startDate={startDate}
				endDate={endDate}
				setStartDate={setStartDate}
				setEndDate={setEndDate}
			/>
			<Row>
				<Col lg={6} className="mt-5">
					<h4 className="text-center">
						<span className="text-success">Income Chart</span>
					</h4>
					<PieChart 
						labels={incomeBreakdown.labels}
						data={incomeBreakdown.data}
						numberOfColors={incomeBreakdown.count}
					/>
				</Col>
				<Col lg={6} className="mt-5">
					<h4 className="text-center">
						<span className="text-danger">Expense Chart</span>
					</h4>
					<PieChart 
						labels={expenseBreakdown.labels}
						data={expenseBreakdown.data}
						numberOfColors={expenseBreakdown.count}
					/>
				</Col>
			</Row>
		</Fragment>
	)
}