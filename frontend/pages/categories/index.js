import { Fragment, useState, useEffect, useContext } from 'react';
import { Table, Row, Col, Button } from 'react-bootstrap';
import Link from 'next/link';
import UserContext from '../../contexts/UserContext';

export default function index() {
	const [ tableBody, setTableBody] = useState([]);
	const { user } = useContext(UserContext);

	const backendAPI = process.env.NEXT_PUBLIC_BACKEND_API || '';

	useEffect(() => {
		getCategories();
	}, [user]);

	async function deleteCategory(categoryId) {
		const confirmation = 
			confirm('This will also delete associated records, proceed?');
		if (confirmation === false) return;

		const route = `${backendAPI}/api/categories/${categoryId}`;
		const token = user.token;

		const deleteCatRes = await fetch(route, {
			method: 'DELETE',
			headers: {
				'Authorization': `Bearer ${token}`
			}
		});
		const deleteCatData = await deleteCatRes.json();

		if (deleteCatData.success) {
			await getCategories();
			alert('Category Successfully Deleted');
		}
	}

	async function getCategories() {
		const token = user.token;
		if (token === null) return;
		const categoriesRes = await fetch(`${backendAPI}/api/categories/all`, {
			headers: {
				'Authorization': `Bearer ${token}`
			}
		});
		const categoriesData = await categoriesRes.json();
		const categories = categoriesData.success;
		setTableBody(categories.map(category => {
			const textColor = category.budgetType === 'income'
				? 'text-success'
				: 'text-danger'

			return (
				<tr key={category._id}>
					<td className="align-middle">{category.name}</td>
					<td className={`align-middle ${textColor}`}>
						{category.budgetType[0].toUpperCase() + category.budgetType.slice(1)}
					</td>
					<td>
						<Row>
							<Link href={`/categories/edit/${category._id}`}>
								<Button variant="warning" className="w-100">Edit</Button>
							</Link>
						</Row>
						<Row className="mt-1">
							<Button 
								variant="danger"
								className="w-100"
								onClick={() => deleteCategory(category._id)}
							>
								Delete
							</Button>
						</Row>
					</td>
				</tr>
			)
		}));
	}

	return (
		<Fragment>
			<h4>Categories</h4>
			<Link href="/categories/new">
				<a className="btn btn-primary" type="button">Add</a>
			</Link>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Category</th>
						<th>Type</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{ tableBody }
				</tbody>
			</Table>
		</Fragment>
	)
}