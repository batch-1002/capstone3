import { Fragment, useContext } from 'react';
import Head from 'next/head';

import styles from '../styles/Home.module.css';
import UserContext from '../contexts/UserContext';
import Login from './login/index';
import Records from './records/index';

export default function Home() {
	const { user } = useContext(UserContext);

	return (
		<Fragment>
		  	{(user.token !== null)
	  			? <Records />
	  			: <Login />
	  		}
		</Fragment>
  	)
}
