function addTrailingZero(num) {
	let numString = num.toString();
	if (numString.length <= 1) numString = '0' + numString;
	return numString;
}

function dateObjToDateControl(dateObj) {
	const year = dateObj.getFullYear();
	const month = parseInt(dateObj.getMonth());
	const day = dateObj.getDate();

	return `${year}-${addTrailingZero(month + 1)}-${addTrailingZero(day)}`
}

function generateStartEndControlDate(divider) {
	divider = divider || 2;
	const endDateObj = new Date();
	const endYear = endDateObj.getFullYear();
	const endMonth = parseInt(endDateObj.getMonth());
	const endDay = endDateObj.getDate();

	const startDateObj = new Date(endYear, parseInt(endMonth - divider), endDay);

	return {
		startDate: dateObjToDateControl(startDateObj),
		endDate: dateObjToDateControl(endDateObj)
	}
}

function dateControlToDateISO(dateControl) {
	let [year, month, day] = dateControl.split('-');
	month = parseInt(month) - 1;
	return (new Date(year, month, day)).toISOString();
}

export default { 
	dateObjToDateControl,
	dateControlToDateISO,
	generateStartEndControlDate
}